	function Pokemon(name, level, hp, power){

		this.name = name;
		this.level = level;
		this.health = hp * level;
		this.attack = power * level / 10;

		this.tackle = function(target){
			target.health -= this.attack;
			console.log(`${this.name} tackled ${target.name}`);
			if (target.health > 10) {
				console.log(`${target.name}'s health is now reduced to ${target.health}`);		
			} else {
				console.log(`${target.name}'s health is now reduced to 0`)
				target.faint()
			};
		};
		this.faint = function(){
			console.log(`${this.name} fainted`)
		} 
	}

let mewtwo = new Pokemon ("Mewtwo", 100, 300,50);
let pidgey = new Pokemon ("Pidgey", 20, 60, 10);
